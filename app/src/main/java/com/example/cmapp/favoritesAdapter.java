package com.example.cmapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class favoritesAdapter extends RecyclerView.Adapter<favoritesAdapter.BookViewHolder> {

    // creating variables for arraylist and context.
    private Context mcontext;
    private Intent i;
    private List<String> lstName, lstUrl;
    // creating constructor for array list and context.
    public favoritesAdapter(List<String> lstName, List<String> lstUrl, Context mcontext) {
        this.lstName = lstName;
        this.lstUrl = lstUrl;
        this.mcontext = mcontext;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        System.out.println("AQUIIIIIIIII"+lstName.toString());
        // inflating our layout for item of recycler view item.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.display_fav, parent, false);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {

        // inside on bind view holder method we are
        // setting ou data to each UI component.
        System.out.println("POSIÇÃO "+position);
        System.out.println("lstName.get(position): "+lstName.get(position));

        holder.nameTV.setText(lstName.get(position));
        System.out.println("EISDFIWOSDJFIWOSJ"+lstName);
        // below line is use to set image from URL in our image view.
        Picasso.get().load(lstUrl.get(position)).into(holder.bookIV);

        // below line is use to add on click listener for our item of recycler view.
        /*holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // inside on click listener method we are calling a new activity
                // and passing all the data of that item in next intent.
                i = new Intent(mcontext, BookDetails.class);
                i.putExtra("title", bookInfo.getTitle());
                i.putExtra("subtitle", bookInfo.getSubtitle());
                i.putExtra("authors", bookInfo.getAuthors());
                i.putExtra("publisher", bookInfo.getPublisher());
                i.putExtra("publishedDate", bookInfo.getPublishedDate());
                i.putExtra("description", bookInfo.getDescription());
                i.putExtra("pageCount", bookInfo.getPageCount());
                i.putExtra("thumbnail", bookInfo.getThumbnail());
                i.putExtra("previewLink", bookInfo.getPreviewLink());
                i.putExtra("infoLink", bookInfo.getInfoLink());
                i.putExtra("buyLink", bookInfo.getBuyLink());
                // after passing that data we are
                // starting our new intent.
                i.addFlags(i.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(i);
            }
        });*/
    }
    @Override
    public int getItemCount() {
        // inside get item count method we
        // are returning the size of our array list.
        return lstUrl.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        // below line is use to initialize
        // our text view and image views.
        TextView nameTV;
        ImageView bookIV;

        public BookViewHolder(View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.bookTitle);
            bookIV = itemView.findViewById(R.id.bookPic);
        }
    }
}
