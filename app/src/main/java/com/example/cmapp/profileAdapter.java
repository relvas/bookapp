package com.example.cmapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class profileAdapter extends RecyclerView.Adapter<profileAdapter.myviewHolder>{
    ArrayList<profileModel> profileHolder;
    Fragment frag;

    public profileAdapter(ArrayList<profileModel> profileHolder) {
        this.profileHolder = profileHolder;
    }

    @NonNull
    @Override
    public myviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_design,parent,false);
        return new myviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myviewHolder holder, int position) {
        holder.options.setText(profileHolder.get(position).getOptions());
        
        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(position){
                    case 0:
                        frag = new ReadList();
                        break;
                    case 1:
                        frag = new favoritesFragment();
                        break;
                    case 2:
                        frag = new BooksFragment();
                        break;
                }
                FragmentManager manager= ((AppCompatActivity)v.getContext()).getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.container, frag).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return profileHolder.size();
    }

    class myviewHolder extends RecyclerView.ViewHolder {
        Button options;
        public myviewHolder(@NonNull View itemView) {
            super(itemView);
            options=itemView.findViewById(R.id.profile_options);
        }
    }
}
