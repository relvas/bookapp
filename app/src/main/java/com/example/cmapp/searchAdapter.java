
package com.example.cmapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class searchAdapter extends RecyclerView.Adapter<searchAdapter.BookViewHolder> {

    // creating variables for arraylist and context.
    private ArrayList<BookInfo> bookInfoArrayList;
    private Context mcontext;
    private Intent i;
    // creating constructor for array list and context.
    public searchAdapter(ArrayList<BookInfo> bookInfoArrayList, Context mcontext) {
        this.bookInfoArrayList = bookInfoArrayList;
        this.mcontext = mcontext;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // inflating our layout for item of recycler view item.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_rv_item, parent, false);
        return new BookViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {

        // inside on bind view holder method we are
        // setting ou data to each UI component.
        BookInfo bookInfo = bookInfoArrayList.get(position);

        holder.nameTV.setText(bookInfo.getTitle());
        holder.publisherTV.setText(bookInfo.getPublisher());
        holder.pageCountTV.setText("No of Pages : " + bookInfo.getPageCount());
        holder.dateTV.setText(bookInfo.getPublishedDate());
        holder.CL.setTag(bookInfo.getTitle() + ";"  + bookInfo.getSubtitle() + ";" + bookInfo.getAuthors() + ";" + bookInfo.getDescription()  + ";" + ("https://" + bookInfo.getThumbnail().split("://")[1]) + ";" + bookInfo.getPublishedDate()+ ";");

        // below line is use to set image from URL in our image view.
        System.out.println(bookInfo.getThumbnail());
        Picasso.get().load("https://" + bookInfo.getThumbnail().split("://")[1]).into(holder.bookIV);
    }
    @Override
    public int getItemCount() {
        // inside get item count method we
        // are returning the size of our array list.
        return bookInfoArrayList.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // below line is use to initialize
        // our text view and image views.
        TextView nameTV, publisherTV, pageCountTV, dateTV;
        ImageView bookIV;
        searchAdapter adapter;
        ConstraintLayout CL ;
        public BookViewHolder(View itemView, searchAdapter adapter ) {
            super(itemView);
            this.adapter = adapter;
            nameTV = itemView.findViewById(R.id.idTVBookTitle);
            publisherTV = itemView.findViewById(R.id.idTVpublisher);
            pageCountTV = itemView.findViewById(R.id.idTVPageCount);
            dateTV = itemView.findViewById(R.id.idTVDate);
            bookIV = itemView.findViewById(R.id.idIVbook);
            CL = itemView.findViewById(R.id.dfhjk);
            CL.setOnClickListener(this);
        }
        @Override
        public void onClick(View view){
            final String tag = "" + view.getTag();
            System.out.println(tag);
            String [] bookinfo = tag.split(";");
            i = new Intent(view.getContext(), BookDetails.class);
            i.putExtra("title", bookinfo[0]);
            i.putExtra("subtitle", bookinfo[1]);
            i.putExtra("authors",  bookinfo[2]);
            i.putExtra("description",  bookinfo[3]);
            i.putExtra("thumbnail",  bookinfo[4]);
            i.putExtra("publishedDate",  bookinfo[5]);

            // after passing that data we are
            // starting our new intent.
            i.addFlags(i.FLAG_ACTIVITY_NEW_TASK);
            mcontext.startActivity(i);
        }
    }

}