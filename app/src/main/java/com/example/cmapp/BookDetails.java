package com.example.cmapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class BookDetails extends AppCompatActivity {
    String title, publishedDate, description, thumbnail, userID, comment;
    int pageCount;
    private ArrayList<String> authors;
    TextView titleTV, descTV, publishDateTV, commentTV;
    ImageView bookIV;
    private ImageButton imgButton, checkButton, starButton, sendBtn;
    private FirebaseFirestore mStore;
    private FirebaseAuth mAuth;

    //LOCATION
    FusedLocationProviderClient mFusedLocationClient;
    TextView tv;
    int PERMISSION_ID = 44;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        // initializing our views..
        titleTV = findViewById(R.id.idTVTitle);
        descTV = findViewById(R.id.idTVDescription);
        publishDateTV = findViewById(R.id.idTVPublishDate);
        bookIV = findViewById(R.id.idIVbook);
        imgButton = findViewById(R.id.plusButton);
        checkButton = findViewById(R.id.checkButton);
        starButton = findViewById(R.id.starButton);
        sendBtn = findViewById(R.id.sendButton);
        commentTV = (EditText) findViewById(R.id.comment_test);

        // getting the data which we have passed from our adapter class.
        title = getIntent().getStringExtra("title");
        publishedDate = getIntent().getStringExtra("publishedDate");
        description = getIntent().getStringExtra("description");
        thumbnail = getIntent().getStringExtra("thumbnail");

        // after getting the data we are setting
        // that data to our text views and image view.
        comment = commentTV.getText().toString();
        System.out.println(("OKASODSJAODSJSAOJSD" + comment));
        titleTV.setText(title);
        publishDateTV.setText("Published On : " + publishedDate);
        descTV.setText(description);
        Picasso.get().load(thumbnail).into(bookIV);

        mAuth = FirebaseAuth.getInstance();
        mStore = FirebaseFirestore.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        DocumentReference bRef = mStore.collection("BooksToRead").document(userID);
        DocumentReference fRef = mStore.collection("Favorites").document(userID);
        DocumentReference rRef = mStore.collection("BooksRead").document(userID);
        List<String> titleListBookToRead = new ArrayList<>();
        List<String> urlListBookToRead = new ArrayList<>();
        List<String> titleListBookRead = new ArrayList<>();
        List<String> urlListBookRead = new ArrayList<>();
        List<String> titleListFavorites = new ArrayList<>();
        List<String> urlListFavorites = new ArrayList<>();

        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleListBookToRead.add(title);
                urlListBookToRead.add(thumbnail);
                HashMap<String, Object> str = new HashMap<>();
                str.put("title", titleListBookToRead);
                str.put("img", urlListBookToRead);
                Toast.makeText(getApplicationContext(), "Book added to Read later!", Toast.LENGTH_SHORT).show();
                bRef.set(str, SetOptions.merge());
            }
        });
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleListBookRead.add(title);
                urlListBookRead.add(thumbnail);
                HashMap<String, Object> str = new HashMap<>();
                str.put("title", titleListBookRead);
                str.put("img", urlListBookRead);
                Toast.makeText(getApplicationContext(), "Book Read!", Toast.LENGTH_SHORT).show();
                rRef.set(str, SetOptions.merge());
            }
        });
        starButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleListFavorites.add(title);
                urlListFavorites.add(thumbnail);
                HashMap<String, Object> str = new HashMap<>();
                str.put("title", titleListFavorites);
                str.put("img", urlListFavorites);
                Toast.makeText(getApplicationContext(), "Marked as favorite!", Toast.LENGTH_SHORT).show();
                fRef.set(str, SetOptions.merge());
            }
        });
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> str = new HashMap<>();
                str.put("ID", userID);
                str.put("title", title);
                System.out.println("ANTES DE DAR MERDA" + comment);
                str.put("comment", comment);
                Toast.makeText(getApplicationContext(), "Comment add with success!", Toast.LENGTH_SHORT).show();
                mStore.collection("Coments").document(userID).set(str).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("DEPOIS DE LIMPARA" + comment);
                        System.out.println("Succeed!!");
                    }
                });
            }
        });

        //LOCATION
        tv = findViewById(R.id.textView4);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
    }

    //Location
    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        // check if permissions are given
        if (checkPermissions()) {

            // check if location is enabled
            if (isLocationEnabled()) {

                // getting last
                // location from
                // FusedLocationClient
                // object
                mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location == null) {
                            requestNewLocationData();
                        } else {
                            //tv.setText("lat: "+location.getLatitude() + "long: " + location.getLongitude());
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            String cityName = addresses.get(0).getAddressLine(0);
                            tv.setText(cityName);
                        }
                    }
                });
            } else {
                Toast.makeText(this, "Please turn on" + " your location...", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            // if permissions aren't available,
            // request for permissions
            requestPermissions();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        // Initializing LocationRequest
        // object with appropriate methods
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        // setting LocationRequest
        // on FusedLocationClient
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            //tv.setText("lat: "+mLastLocation.getLatitude() + "long: " + mLastLocation.getLongitude());
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String cityName = addresses.get(0).getAddressLine(0);

            tv.setText(cityName);
        }
    };

    // method to check for permissions
    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        // If we want background location
        // on Android 10.0 and higher,
        // use:
        // ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
    }


    // method to request for permissions
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID);
    }

    // method to check
    // if location is enabled
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    // If everything is alright then
    @Override
    public void
    onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }
    }
}